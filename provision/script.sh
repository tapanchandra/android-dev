#!/bin/sh
#=========================================================

#=========================================================
echo "Install Packages..."
#=========================================================
sudo apt-get update
sudo apt-get install unzip lib32z1 lib32ncurses5 lib32bz2-1.0 lib32stdc++6 > /dev/null

#=========================================================
echo "Install Desktop Environment..."
#=========================================================
sudo apt-get update
sudo apt-get install -y --no-install-recommends xubuntu-desktop > /dev/null


#=========================================================
echo "Install Git..."
#=========================================================
sudo apt-get install git -y > /dev/null


#=========================================================
echo "Add PPA repositories..."
#=========================================================
sudo apt-add-repository ppa:paolorotolo/android-studio
sudo add-apt-repository ppa:webupd8team/java
sudo add-apt-repository ppa:webupd8team/sublime-text-3
sudo apt-get update > /dev/null


#=========================================================
echo "Install Java..."
#=========================================================
sudo apt-get install -y python-software-properties debconf-utils > /dev/null
echo "oracle-java8-installer shared/accepted-oracle-license-v1-1 select true" | sudo debconf-set-selections > /dev/null
sudo apt-get install -y oracle-java8-installer > /dev/null
sudo apt-get install -y oracle-java8-set-default  > /dev/null

#=========================================================
echo "Install Sublime Text 3..."
#=========================================================
sudo apt-get install sublime-text-installer > /dev/null


#=========================================================
echo "Download the latest chrome..."
#=========================================================
wget "https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
sudo dpkg -i google-chrome-stable_current_amd64.deb > /dev/null
sudo rm google-chrome-stable_current_amd64.deb
sudo apt-get install -y -f > /dev/null

#=========================================================
echo "Install android studio..."
#=========================================================
sudo apt-get install android-studio -y > /dev/null
#sh /opt/android-studio/bin/studio.sh > /dev/null

#=========================================================
echo "Install Genymotion..."
#=========================================================


#=========================================================
echo "Install Desktop Shortcuts..."
#=========================================================



#=========================================================
echo "Reboot the VM"
#=========================================================
sudo reboot